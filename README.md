Bamboo-install
=========

Role is used to install desired version of Atlassian Bamboo to Debian based system. It's
currently tested on Debian 9 systems. Role will start Bamboo as a systemd service. 

Requirements
------------

This role does not need any external dependencies


Role Variables
--------------

Defaults: 
```
jdk_location: /usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/bin/java
bamboo_version: "6.7.2"

bamboo_context_root: ""
```

If you want to automatically configure PostgreSQL database and user for Bamboo use
following variables:

```
bamboo_context_root: [String]   # Context root for Bamboo application; leave empty if you
                                  aren't using it behind proxy
bamboo_password: [String]       # Password for the Bamboo user on the machine; it's
                                  recommended to put this value to vault
configure_postgre: [Boolean]    # If this variable is set installer will configure user and
                                  database for Bamboo at given database host

bamboo_db_name: [String]        # name of database for Bamboo
bamboo_db_user: [String]        # username of Bamboo database user
bamboo_db_password: [String]    # password for Bamboo database user; it's recommended to put
                                  this value to vault

pg_admin_user: [String]         # admin user that has permission to create new users and
                                  databases at given host 
pg_admin_password: [String]     # admin user password; it's recommended to put this value
                                  to vault 
pg_host: [String]               # database host where Bamboo database will be created
```

Example Playbook
----------------

Example playbook and inventory file is provided in ./test directory. Testing of this role
can be executed using Vagrant. Position yourself to root directory of role and execute
`vagrant up` command.

License
-------

Apache License, Version 2.0

Author Information
------------------
Matija Bartolac  
CROZ d.o.o  
mbartolac@croz.net  
gitlab.com/kdmk  
  
You can find more roles at gitlab.com/anisble-roles
